package com.mahadevaz_10191046.googlefotoandcontact;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<SetterGetter> datamenu;
    GridLayoutManager gridLayoutManager;
    DashBoardAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.rlmenu);
        addData();
        gridLayoutManager = new GridLayoutManager(this,3);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new DashBoardAdapter(datamenu);
        recyclerView.setAdapter(adapter);
    }

    public void addData(){
        datamenu = new ArrayList<>();
        datamenu.add(new SetterGetter("syeif",
                "logomenu1"));
        datamenu.add(new SetterGetter("syifa", "logomenu2"));
        datamenu.add(new SetterGetter("Iqbal", "logomenu3"));
        datamenu.add(new SetterGetter("cania", "logomenu4"));
        datamenu.add(new SetterGetter("Fikri", "logomenu5"));
        datamenu.add(new SetterGetter("Lully", "logomenu6"));
        datamenu.add(new SetterGetter("Wabdi", "logomenu7"));
        datamenu.add(new SetterGetter("Dian", "logomenu8"));
        datamenu.add(new SetterGetter("Suci", "logomenu9"));
    }
}